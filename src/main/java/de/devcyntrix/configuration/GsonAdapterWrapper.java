package de.devcyntrix.configuration;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import de.devcyntrix.configuration.adapter.Adapter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class GsonAdapterWrapper<I> extends TypeAdapter<I> {

    private final Adapter<I> adapter;

    protected GsonAdapterWrapper(@NotNull Adapter<I> adapter) {
        this.adapter = adapter;
    }

    @Override
    public void write(JsonWriter jsonWriter, I i) throws IOException {
        String write = adapter.write(i);
        jsonWriter.value(write);
    }

    @Override
    public I read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }
        String s = in.nextString();
        return adapter.read(s);
    }

    public static <T> TypeAdapter<T> of(Adapter<T> adapter) {
        return new GsonAdapterWrapper<>(adapter);
    }
}
