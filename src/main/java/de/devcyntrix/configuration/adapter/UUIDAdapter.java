package de.devcyntrix.configuration.adapter;

import java.util.UUID;

public class UUIDAdapter implements Adapter<UUID> {

    @Override
    public String write(UUID value) {
        return value.toString();
    }

    @Override
    public UUID read(String value) {
        return UUID.fromString(value);
    }
}
