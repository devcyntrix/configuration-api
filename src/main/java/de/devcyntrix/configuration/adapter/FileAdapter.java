package de.devcyntrix.configuration.adapter;

import java.io.File;


public class FileAdapter implements Adapter<File> {

    @Override
    public String write(File value) {
        return value.getPath();
    }

    @Override
    public File read(String value) {
        return new File(value);
    }
}
