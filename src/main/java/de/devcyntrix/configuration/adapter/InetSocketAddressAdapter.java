package de.devcyntrix.configuration.adapter;

import java.net.InetSocketAddress;

public class InetSocketAddressAdapter implements Adapter<InetSocketAddress> {

    @Override
    public String write(InetSocketAddress value) {
        return value == null ? null : value.getAddress().getHostAddress() + ":" + value.getPort();
    }

    @Override
    public InetSocketAddress read(String address) {

        String hostname = address.substring(0, address.lastIndexOf(':'));
        String port = address.substring(address.lastIndexOf(':') + 1);

        return new InetSocketAddress(hostname, Integer.parseInt(port));
    }
}
