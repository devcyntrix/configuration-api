package de.devcyntrix.configuration.adapter;

import de.devcyntrix.configuration.Configuration;
import de.devcyntrix.configuration.serialization.Serializable;
import de.devcyntrix.configuration.serialization.SerializationContext;

import java.lang.reflect.Type;
import java.util.Map;

public class ConfigurationSerializer implements Serializable<Configuration> {

    @Override
    public Object serialize(Configuration value, Type fromType, SerializationContext context) {
        return value.getSelf();
    }
}
