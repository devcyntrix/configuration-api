package de.devcyntrix.configuration.adapter;

public interface Adapter<I> {

    String write(I value);

    I read(String value);

}
