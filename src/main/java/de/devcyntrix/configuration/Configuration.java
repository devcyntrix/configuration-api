package de.devcyntrix.configuration;

import de.devcyntrix.configuration.adapter.Adapter;
import de.devcyntrix.configuration.adapter.FileAdapter;
import de.devcyntrix.configuration.adapter.InetSocketAddressAdapter;
import de.devcyntrix.configuration.adapter.UUIDAdapter;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class Configuration implements ConfigurationSerializable {

    public static final Map<Class<?>, Adapter<?>> ADAPTER_MAP = new HashMap<>();

    static {
        ADAPTER_MAP.put(File.class, new FileAdapter());
        ADAPTER_MAP.put(InetSocketAddress.class, new InetSocketAddressAdapter());
        ADAPTER_MAP.put(UUID.class, new UUIDAdapter());
        ADAPTER_MAP.put(int.class, new Adapter<Number>() {
            @Override
            public String write(Number value) {
                return value.toString();
            }

            @Override
            public Number read(String value) {
                return new BigDecimal(value).intValue();
            }
        });
        ADAPTER_MAP.put(String.class, new Adapter<String>() {
            @Override
            public String write(String value) {
                return value;
            }

            @Override
            public String read(String value) {
                return value;
            }
        });
    }

    public static <T> void registerAdapter(Class<T> clazz, Adapter<T> adapter) {
        if (clazz == null)
            throw new IllegalArgumentException();
        ADAPTER_MAP.put(clazz, adapter);
    }

    public static <T> Adapter<T> getAdapter(Type clazz) {
        return (Adapter<T>) ADAPTER_MAP.get(clazz);
    }

    private static final char SEPARATOR = '.';
    @NotNull
    final Map<String, Object> self;
    private final Configuration defaults;

    public Configuration() {
        this(null);
    }

    public Configuration(Configuration defaults) {
        this(new LinkedHashMap<>(), defaults);
    }

    public Configuration(@NotNull Map<String, Object> map, Configuration defaults) {
        this.self = new LinkedHashMap<>();
        this.defaults = defaults;

        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = (entry.getKey() == null) ? "null" : entry.getKey();
            if (entry.getValue() instanceof Map) {
                this.self.put(key, new Configuration((Map) entry.getValue(), (defaults == null) ? null : defaults.getSection(key)));
            } else {
                this.self.put(key, entry.getValue());
            }
        }
    }

    private Configuration getSectionFor(String path) {
        int index = path.indexOf(SEPARATOR);
        if (index == -1) {
            return this;
        }
        String root = path.substring(0, index);
        Object section = self.get(root);
        if (section == null) {
            section = new Configuration((defaults == null) ? null : defaults.getSection(root));
            self.put(root, section);
        }

        return (Configuration) section;
    }

    private String getChild(String path) {
        int index = path.indexOf(SEPARATOR);
        return (index == -1) ? path : path.substring(index + 1);
    }

    public <T> T get(String path, T def) {
        Configuration section = getSectionFor(path);
        Object val;
        if (section == this) {
            val = self.get(path);
        } else {
            val = section.get(getChild(path), def);
        }

        if (val == null && def instanceof Configuration) {
            self.put(path, def);
        }

        return (val != null) ? (T) val : def;
    }

    public Conversion getAndConvert(String path) {
        return Conversion.of(get(path));
    }

    public boolean contains(String path) {
        return get(path, null) != null;
    }

    public Object getDefault(String path) {
        return (defaults == null) ? null : defaults.get(path);
    }

    public Object get(String path) {
        return get(path, getDefault(path));
    }

    public void set(String path, Object value) {
        if (value instanceof Map) {
            value = new Configuration((Map) value, (defaults == null) ? null : defaults.getSection(path));
        }

        Configuration section = getSectionFor(path);
        if (section == this) {
            if (value == null) {
                self.remove(path);
            } else {
                Adapter adapter = ADAPTER_MAP.get(value.getClass());
                if(adapter != null) {
                    value = adapter.write(value);
                }
                self.put(path, value);
            }
        } else {
            section.set(getChild(path), value);
        }
    }

    public <T> void set(String path, T value, @Nullable BiConsumer<T, Configuration> consumer) {
        if(consumer == null) {
            set(path, value);
            return;
        }
        Configuration configuration = new Configuration();
        consumer.accept(value, configuration);
        set(path, configuration);
    }

    public <T> void setList(String path, List<T> list, Function<T, Object> function) {
        setArray(path, function, (T[]) list.toArray());
    }

    public <T> void setArray(String path, Function<T, Object> function, T... items) {
        List<Object> list = new ArrayList<>(items.length);
        for (T item : items) {
            list.add(function.apply(item));
        }
        set(path, list);
    }

    public <T> void setArray(String path, BiConsumer<T, Configuration> consumer, T... items) {
        List<Configuration> list = new ArrayList<>(items.length);
        for (T item : items) {
            Configuration configuration = new Configuration();
            consumer.accept(item, configuration);
            list.add(configuration);
        }
        set(path, list);
    }

    public void put(Configuration configuration) {
        put(null, configuration);
    }

    public void put(@Nullable String path, Configuration configuration) {
        if (configuration.isEmpty())
            return;
        Configuration section = this;
        if (path != null)
            section = getSection(path);
        else
            path = "";

        Collection<String> keys = configuration.getKeys();
        for (String key : keys) {
            String p = (path.isEmpty() ? "" : path + ".") + key;
            Object o = configuration.get(key);
            if (o instanceof Configuration) {
                Configuration childConfiguration = (Configuration) o;
                if (!childConfiguration.isEmpty())
                    section.put(p, childConfiguration);
                else
                    section.set(key, childConfiguration);
            } else {
                set(p, o);
            }
        }
    }

    public Configuration getSection(String path) {
        Object def = getDefault(path);
        return (Configuration) get(path, (def instanceof Configuration) ? def : new Configuration((defaults == null) ? null : defaults.getSection(path)));
    }

    /**
     * Gets keys, not deep by default.
     *
     * @return top level keys for this section
     */
    public Collection<String> getKeys() {
        return new LinkedHashSet<>(self.keySet());
    }

    public byte getByte(String path) {
        Object def = getDefault(path);
        return getByte(path, (def instanceof Number) ? ((Number) def).byteValue() : 0);
    }

    public byte getByte(String path, byte def) {
        Object val = get(path, def);
        return (val instanceof Number) ? ((Number) val).byteValue() : def;
    }

    public List<Byte> getByteList(String path) {
        List<?> list = getList(path);
        List<Byte> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Number) {
                result.add(((Number) object).byteValue());
            }
        }

        return result;
    }

    public short getShort(String path) {
        Object def = getDefault(path);
        return getShort(path, (def instanceof Number) ? ((Number) def).shortValue() : 0);
    }

    public short getShort(String path, short def) {
        Object val = get(path, def);
        return (val instanceof Number) ? ((Number) val).shortValue() : def;
    }

    public List<Short> getShortList(String path) {
        List<?> list = getList(path);
        List<Short> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Number) {
                result.add(((Number) object).shortValue());
            }
        }

        return result;
    }

    public int getInt(String path) {
        Object def = getDefault(path);
        return getInt(path, (def instanceof Number) ? ((Number) def).intValue() : 0);
    }

    public int getInt(String path, int def) {
        Object val = get(path, def);
        return (val instanceof Number) ? ((Number) val).intValue() : def;
    }

    public List<Integer> getIntList(String path) {
        List<?> list = getList(path);
        List<Integer> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Number) {
                result.add(((Number) object).intValue());
            }
        }

        return result;
    }

    public long getLong(String path) {
        Object def = getDefault(path);
        return getLong(path, (def instanceof Number) ? ((Number) def).longValue() : 0);
    }

    public long getLong(String path, long def) {
        Object val = get(path, def);
        return (val instanceof Number) ? ((Number) val).longValue() : def;
    }

    public List<Long> getLongList(String path) {
        List<?> list = getList(path);
        List<Long> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Number) {
                result.add(((Number) object).longValue());
            }
        }

        return result;
    }

    public float getFloat(String path) {
        Object def = getDefault(path);
        return getFloat(path, (def instanceof Number) ? ((Number) def).floatValue() : 0);
    }

    public float getFloat(String path, float def) {
        Object val = get(path, def);
        return (val instanceof Number) ? ((Number) val).floatValue() : def;
    }

    public List<Float> getFloatList(String path) {
        List<?> list = getList(path);
        List<Float> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Number) {
                result.add(((Number) object).floatValue());
            }
        }

        return result;
    }

    public double getDouble(String path) {
        Object def = getDefault(path);
        return getDouble(path, (def instanceof Number) ? ((Number) def).doubleValue() : 0);
    }

    public double getDouble(String path, double def) {
        Object val = get(path, def);
        return (val instanceof Number) ? ((Number) val).doubleValue() : def;
    }

    public List<Double> getDoubleList(String path) {
        List<?> list = getList(path);
        List<Double> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Number) {
                result.add(((Number) object).doubleValue());
            }
        }

        return result;
    }

    public boolean getBoolean(String path) {
        Object def = getDefault(path);
        return getBoolean(path, (def instanceof Boolean) ? (Boolean) def : false);
    }

    public boolean getBoolean(String path, boolean def) {
        Object val = get(path, def);
        return (val instanceof Boolean) ? (Boolean) val : def;
    }

    public List<Boolean> getBooleanList(String path) {
        List<?> list = getList(path);
        List<Boolean> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Boolean) {
                result.add((Boolean) object);
            }
        }

        return result;
    }

    public char getChar(String path) {
        Object def = getDefault(path);
        return getChar(path, (def instanceof Character) ? (Character) def : '\u0000');
    }

    public char getChar(String path, char def) {
        Object val = get(path, def);
        return (val instanceof Character) ? (Character) val : def;
    }

    public List<Character> getCharList(String path) {
        List<?> list = getList(path);
        List<Character> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof Character) {
                result.add((Character) object);
            }
        }

        return result;
    }

    public String getString(String path) {
        Object def = getDefault(path);
        return getString(path, (def instanceof String) ? (String) def : "");
    }

    public String getString(String path, String def) {
        Object val = get(path, def);
        return (val instanceof String) ? (String) val : def;
    }

    public List<String> getStringList(String path) {
        List<?> list = getList(path);
        List<String> result = new ArrayList<>();

        for (Object object : list) {
            if (object instanceof String) {
                result.add((String) object);
            }
        }

        return result;
    }

    public List<?> getList(String path) {
        Object def = getDefault(path);
        return getList(path, (def instanceof List<?>) ? (List<?>) def : Collections.EMPTY_LIST);
    }

    public List<?> getList(String path, List<?> def) {
        Object val = get(path, def);
        if (val instanceof List) {
            List list = (List) val;
            for (int i = 0; i < list.size(); i++) {
                Object o = list.get(i);
                if (o instanceof Map) {
                    Configuration configuration = null;
                    List<?> defaultsList = (List<?>) getDefault(path);
                    if (defaultsList != null && defaultsList.get(i) != null) {
                        configuration = (Configuration) defaultsList.get(i);
                    }

                    list.set(i, new Configuration((Map) o, (Configuration) configuration));
                }
            }
            return list;
        }

        return def;
    }

    public <T> List<T> getListAndConvert(String path, Function<Object, T> function) {
        List<?> list = getList(path);
        List<T> out = new ArrayList<>(list.size());
        for (Object o : list) {
            T apply = function.apply(o);
            out.add(apply);
        }
        return out;
    }

    public boolean isEmpty() {
        return this.self.isEmpty();
    }

    @Override
    public String toString() {
        return this.self.toString();
    }

    public Map<String, Object> getSelf() {
        return self;
    }
}
