package de.devcyntrix.configuration.annotation;

import com.google.common.io.Files;
import com.google.gson.JsonObject;
import de.devcyntrix.configuration.Configuration;
import de.devcyntrix.configuration.ConfigurationProvider;
import de.devcyntrix.configuration.JsonConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.function.BiConsumer;

public abstract class AbstractClassConfiguration implements ConfigurationSerializable {

    boolean missingValues = false;
    private ConfigurationProvider provider;

    public AbstractClassConfiguration(Class<? extends ConfigurationProvider> providerClass) {
        this.provider = ConfigurationProvider.getProvider(providerClass);
    }

    public AbstractClassConfiguration() {
        this(JsonConfiguration.class);
    }

    public static void walkPropertyFields(Field[] fields, BiConsumer<Field, ConfigurationProperty> consumer) {
        for (Field field : fields) {

            /* Gives access to the field */
            field.setAccessible(true);
            /* Skips field without annotation */
            if (!field.isAnnotationPresent(ConfigurationProperty.class))
                continue;
            ConfigurationProperty property = field.getDeclaredAnnotation(ConfigurationProperty.class);
            consumer.accept(field, property);
        }
    }

    /**
     * Loads the configuration file and needs the {@link ConfigurationInfo}
     */
    public void loadFromInfo() {
        ConfigurationInfo info = checkConfigurationInfo();
        File file = new File(info.filename() + provider.getExtension());
        loadFromFile(file);
    }

    public synchronized void loadFromFile(File file) {
        if (!file.isFile()) {
            saveDefaults(file);
            return;
        }

        try {
            Configuration configuration = this.provider.load(new FileReader(file));
            List<String> missing = loadToObject(configuration.getSelf(), this);

            if (!missing.isEmpty()) {
                System.out.println("Missing fields in \"" + file + "\": " + missing);
                save(file);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public <T> T parse(Object o, Field field, @NotNull Type type) throws Exception {
        Class<?> typeClass = null;
        if (type instanceof ParameterizedType) {
            System.out.println(((ParameterizedType) type).getRawType());
            typeClass = Class.forName(((ParameterizedType) type).getRawType().getTypeName());
        } else {
            typeClass = Class.forName(type.getTypeName());
        }

        if (field != null && Collection.class.isAssignableFrom(typeClass) && o instanceof Collection) {

            Type valueType = Object.class;

            if (type instanceof ParameterizedType)
                valueType = ((ParameterizedType) type).getActualTypeArguments()[0];

            System.out.println("Parsing collection");

            Collection collection = (Collection) o;
            List list = new ArrayList<>(collection.size());
            for (Object element : collection) {
                list.add(parse(element, null, valueType));
            }

            return (T) list;
        }

        if (field != null && Map.class.isAssignableFrom(typeClass) && o instanceof Map) {

            Type keyType = Object.class, valueType = Object.class;

            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                keyType = parameterizedType.getActualTypeArguments()[0];
                valueType = parameterizedType.getActualTypeArguments()[1];
            }

            Map<Object, Object> gotMap = (Map) o;
            Map map = new HashMap(gotMap.size());
            for (Map.Entry<Object, Object> entry : gotMap.entrySet()) {
                map.put(
                        parse(entry.getKey(), null, keyType),
                        parse(entry.getValue(), null, valueType)
                );
            }

            return (T) map;
        }


        if (typeClass.isArray() && ConfigurationSerializable.class.isAssignableFrom(typeClass.getComponentType())) {
            return (T) ((Collection) parse(Arrays.asList((Object[]) o), field, type)).toArray();
        }

        if (ConfigurationSerializable.class.isAssignableFrom(typeClass)) {
            if (!(o instanceof Map))
                throw new Error("ConfigurationSerializable element must be a json object!");
            Map<String, Object> map = (Map<String, Object>) o;

            Constructor<?> constructor;
            try {
                constructor = typeClass.getConstructor();
            } catch (NoSuchMethodException e) {
                throw new Error("ConfigurationSerializable class needs an empty constructor! In class " + typeClass.getCanonicalName());
            }
            try {
                T value = (T) constructor.newInstance();
                boolean missingValues = !loadToObject(map, value).isEmpty();
                if (missingValues) {
                    return null;
                }
                return value;
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }
        return (T) o;
    }

    public synchronized List<String> loadToObject(Map<String, Object> map, Object object) {
        System.out.println("Try to set " + map + " to " + object.getClass().getCanonicalName());

        Configuration configuration = new Configuration(map, null);
        List<Field> fieldList = new LinkedList<>();

        Class<?> checkObjectClass = object.getClass();
        do {
            fieldList.addAll(Arrays.asList(checkObjectClass.getDeclaredFields()));
            checkObjectClass = checkObjectClass.getSuperclass();
        } while (ConfigurationSerializable.class.isAssignableFrom(checkObjectClass));

        Field[] fields = fieldList.toArray(new Field[0]);

        ArrayList<String> missingFields = new ArrayList<>();

        walkPropertyFields(fields, (field, property) -> {
            Object o = configuration.getAndConvert(property.name()).convert(field.getGenericType());
            if (o == null) {
                if (!property.optional()) {
                    missingFields.add(property.name());
                }
                return;
            }
            System.out.println(o);

//            Object fieldValue = null;
//            try {
//                fieldValue = parse(o, field, field.getGenericType());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            if (o == null) {
                missingFields.add(property.name());
                return;
            }

            try {
                field.set(object, o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        return missingFields;
    }

    public Set<String> getProperties() {
        Field[] fields = getClass().getDeclaredFields();
        Set<String> set = new HashSet<>();
        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(ConfigurationProperty.class))
                continue;
            ConfigurationProperty property = field.getAnnotation(ConfigurationProperty.class);
            set.add(property.name());
        }
        return set;
    }

    public Object mapObject(Object object) {
        if (object == null)
            return null;

        if (object instanceof ConfigurationSerializable) {

            Map<String, Object> map = new LinkedHashMap<>();
            JsonObject jsonObject = new JsonObject();

            Class<?> clazz = object.getClass();
            Field[] fields = clazz.getDeclaredFields();

            walkPropertyFields(fields, (field, property) -> {
                try {
                    Object value = mapObject(field.get(object));
                    map.put(property.name(), value);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });

            return map;
        }

        if (object instanceof ConfigurationSerializable[]) {
            ConfigurationSerializable[] serializables = (ConfigurationSerializable[]) object;
            List<Object> list = new ArrayList<>(serializables.length);
            for (ConfigurationSerializable serializable : serializables) {
                list.add(mapObject(serializable));
            }

            return list;
        }

        return object;
    }

    private void saveDefaults(File file) {
        try {
            Files.createParentDirs(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, Object> map = (Map<String, Object>) mapObject(this);
        Configuration configuration = new Configuration(map, null);

        try {
            PrintWriter writer = new PrintWriter(file);
            provider.save(configuration, writer);
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the configuration file.
     */
    public synchronized void save() {
        ConfigurationInfo info = checkConfigurationInfo();

        File file = new File(info.filename());
        if (!file.isFile()) {
            saveDefaults(file);
            return;
        }

        save(file);
    }

    public synchronized void save(File file) {
        saveDefaults(file);
    }

    private ConfigurationInfo checkConfigurationInfo() {
        ConfigurationInfo info = getClass().getAnnotation(ConfigurationInfo.class);
        if (info == null) {
            throw new NullPointerException("The configuration info is missing.");
        }
        return info;
    }

}
