package de.devcyntrix.configuration.annotation;

import com.google.gson.*;
import com.google.gson.internal.Primitives;
import de.devcyntrix.configuration.serialization.Serialization;

import java.lang.reflect.Type;
import java.util.Map;

public class SerializationWrapper implements JsonSerializer, JsonDeserializer {

    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        System.out.println("DE " + json);
        Object deserialize = context.deserialize(json, Object.class);
        Object deserialize1 = Serialization.deserialize(deserialize, typeOfT);
        return (deserialize1 != null ? deserialize1: deserialize);
    }

    @Override
    public JsonElement serialize(Object src, Type typeOfSrc, JsonSerializationContext context) {

        Object serialize = Serialization.serialize(src, typeOfSrc);
        if(serialize == null)
            return context.serialize(src, typeOfSrc);

        JsonElement serialize1 = context.serialize(serialize, serialize.getClass());
        return serialize1;
    }
}
