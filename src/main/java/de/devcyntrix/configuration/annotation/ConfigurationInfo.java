package de.devcyntrix.configuration.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * CyConfiguration Project
 * 1.0.0 SNAPSHOT
 *
 * © 2018 Ricardo Borutta
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ConfigurationInfo {

    String filename();

}
