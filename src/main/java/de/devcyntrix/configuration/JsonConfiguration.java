package de.devcyntrix.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import de.devcyntrix.configuration.annotation.SerializationWrapper;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;

public class JsonConfiguration extends ConfigurationProvider {

    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .registerTypeAdapterFactory(new GsonTypeAdapterFactory())
            .registerTypeHierarchyAdapter(ConfigurationSerializable.class, new SerializationWrapper())
            .create();

    @Override
    public void save(Configuration config, Writer writer) {
        gson.toJson(config.self, writer);
        try {
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Configuration load(Reader reader, Configuration defaults) {
        Map<String, Object> map = gson.fromJson(reader, LinkedHashMap.class);
        if (map == null) {
            map = new LinkedHashMap<>();
        }
        return new Configuration(map, defaults);
    }

    @Override
    public String getExtension() {
        return ".json";
    }
}
