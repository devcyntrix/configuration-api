package de.devcyntrix.configuration;

import de.devcyntrix.configuration.adapter.Adapter;
import de.devcyntrix.configuration.serialization.Serialization;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

public class Conversion {

    private Object value;

    protected Conversion(Object value) {
        this.value = value;
    }

    public <T> @Nullable T convert(Type type) {
        if (this.value == null)
            return null;

        System.out.println("Convert to " + type);
        if (!(value instanceof Configuration) && !(value instanceof Collection) && !(value instanceof Map)) {
            Adapter<T> adapter = (Adapter<T>) Configuration.ADAPTER_MAP.get(type);

            if (adapter == null) {
                return (T) this.value;
            }
            return adapter.read(value.toString());
        }
        Object deserialize = Serialization.deserialize(this.value, type);
        if (deserialize == null)
            return (T) value;

        return (T) deserialize;
    }

    public <T> @Nullable T convert(Function<Configuration, T> function) {
        if (!(this.value instanceof Configuration))
            return null;
        return function.apply((Configuration) this.value);
    }

    public static Conversion of(Object value) {
        return new Conversion(value);
    }


}
