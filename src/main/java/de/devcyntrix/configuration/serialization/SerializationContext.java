package de.devcyntrix.configuration.serialization;

import com.google.gson.JsonElement;

import java.lang.reflect.Type;

public interface SerializationContext {

    Object serialize(Object value);

    Object serialize(Object value, Type fromType);

}
