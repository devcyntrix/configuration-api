package de.devcyntrix.configuration.serialization;

import de.devcyntrix.configuration.Configuration;
import de.devcyntrix.configuration.adapter.ConfigurationSerializer;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Serialization {

    private static final Map<Class<?>, Serializable> serializableMap = new HashMap<>();
    private static final Map<Class<?>, Deserializable<?>> deserializableMap = new HashMap<>();

    static {
        serializableMap.put(Configuration.class, new ConfigurationSerializer());
        deserializableMap.put(Collection.class, new Deserializable<Collection>() {
            @Override
            public Collection deserialize(Object value, Type toType, DeserializationContext context) {

                Type valueType = Object.class;

                if(toType instanceof ParameterizedType)
                    valueType = ((ParameterizedType) toType).getActualTypeArguments()[0];

                System.out.println(valueType);

                Collection collection = (Collection) value;
                Type finalValueType = valueType;
                return (List) collection.stream().map(o -> context.deserialize(o, finalValueType)).collect(Collectors.toList());
            }
        });
    }

    public static <T> Serializable<?> registerSerializer(Class<T> typeClass, Serializable<T> serializable) {
        return serializableMap.put(typeClass, serializable);
    }

    public static Serializable<?> unregisterSerializer(Class<?> typeClass) {
        return serializableMap.remove(typeClass);
    }

    public static <T> Deserializable<?> registerDeserializer(Class<T> typeClass, Deserializable<T> deserializable) {
        return deserializableMap.put(typeClass, deserializable);
    }

    public static Deserializable<?> unregisterDeserializer(Class<?> typeClass) {
        return deserializableMap.remove(typeClass);
    }

    public static @Nullable Object deserialize(@NotNull Object value) {
        return deserialize(value, null);
    }

    public static @Nullable Object deserialize(@NotNull Object value, @Nullable Type toType) {
        DeserializationContext deserializationContext = new DeserializationContextImpl();
        Stream<?> stream = deserializableMap.entrySet().stream()
                .filter(classDeserializableEntry -> classDeserializableEntry.getKey().isAssignableFrom(value.getClass()))
                .map(classDeserializableEntry -> classDeserializableEntry.getValue().deserialize(value, toType, deserializationContext))
                .filter(Objects::nonNull);
        if (toType != null)
            stream = stream.filter(o -> o.getClass().equals(toType));
        return stream.findFirst().orElse(null);
    }

    public static @Nullable Object serialize(@NotNull Object value) {
        return serialize(value, value.getClass());
    }

    public static @Nullable Object serialize(@NotNull Object value, @Nullable Type typeOfSrc) {
        SerializationContext serializationContext = new SerializationContextImpl();
        Stream<?> stream = serializableMap.entrySet().stream()
                .filter(classSerializableEntry -> classSerializableEntry.getKey().isAssignableFrom(value.getClass()))
                .map(classSerializableEntry -> classSerializableEntry.getValue().serialize(value, typeOfSrc, serializationContext))
                .filter(Objects::nonNull);
        return stream.findFirst().orElse(null);
    }

    public static class DeserializationContextImpl implements DeserializationContext {

        @Override
        public Object deserialize(Object value) {
            return Serialization.deserialize(value);
        }

        @Override
        public Object deserialize(Object value, Type toType) {
            return Serialization.deserialize(value, toType);
        }
    }

    public static class SerializationContextImpl implements SerializationContext {

        @Override
        public Object serialize(Object value) {
            return Serialization.serialize(value);
        }

        @Override
        public Object serialize(Object value, Type fromType) {
            return Serialization.serialize(value, fromType);
        }
    }

}
