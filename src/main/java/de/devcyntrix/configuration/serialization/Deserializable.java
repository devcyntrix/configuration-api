package de.devcyntrix.configuration.serialization;

import java.lang.reflect.Type;

public interface Deserializable<T> {

    T deserialize(Object value, Type toType, DeserializationContext context);

}
