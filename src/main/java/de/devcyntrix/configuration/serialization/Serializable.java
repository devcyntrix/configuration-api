package de.devcyntrix.configuration.serialization;

import java.lang.reflect.Type;

public interface Serializable<T> {

    Object serialize(T value, Type typeOfSrc, SerializationContext context);

}
