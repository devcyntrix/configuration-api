package de.devcyntrix.configuration.serialization;

import java.lang.reflect.Type;

public interface DeserializationContext {

    Object deserialize(Object value);

    Object deserialize(Object value, Type toType);

}
