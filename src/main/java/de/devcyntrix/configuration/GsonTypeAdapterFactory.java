package de.devcyntrix.configuration;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import de.devcyntrix.configuration.adapter.Adapter;

public class GsonTypeAdapterFactory implements TypeAdapterFactory {
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Adapter<T> adapter = Configuration.getAdapter(typeToken.getType());
        if(adapter == null)
            return null;
        return new GsonAdapterWrapper<>(adapter);
    }
}
